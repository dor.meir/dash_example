import dash
import dash_core_components as dcc
import dash_html_components as html 

app = dash.Dash(__name__) 
app.layout = html.Div(children=[
    html.H1('Heading 1'),
    html.Label('Filter by level:'),
    dcc.Dropdown(
        options=[
            {'label': 'Junior', 'value': 'junior'},
            {'label': 'Mid level', 'value': 'mid'},
            {'label': 'Senior', 'value': 'senior'}
        ],
        value='junior'
    ),
    html.Label('Filter by skills:'),
    dcc.Dropdown(
        options=[
            {'label': 'Python', 'value': 'python'},
            {'label': 'R', 'value': 'r'},
            {'label': 'Machine learning', 'value': 'ml'}
        ],
        value=['python', 'ml'],
        multi=True
    ),
    html.Label('Experience level:'),
    dcc.RadioItems(
        options=[
            {'label': '0-1 years of experience', 'value': '01'},
            {'label': '2-5 years of experience', 'value': '25'},
            {'label': '5+ years of experience', 'value': '5plus'}
        ],
        value='25'
    ),
    html.Label('Additional:'),
    dcc.Checklist(
        options=[
            {'label': 'Married', 'value': 'married'},
            {'label': 'Has kids', 'value': 'haskids'}
        ],
        value=['married']
    ),
    html.Label('Overall impression:'),
    dcc.Slider(
        min=1,
        max=10,
        value=5
    ),
    html.Label('Anything to add?'),
    dcc.Input(type='text')
])


if __name__ == '__main__':
    app.run_server(debug=True)