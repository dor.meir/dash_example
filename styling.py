import dash
import dash_html_components as html 

app = dash.Dash(__name__) 
app.layout = html.Div(
    className='wrapper',
    children=[
        html.H3('Dash App', className='main-title'),
        html.P('A simple Python dashboard', className='paragraph-lead'),
        html.Div(
            className='card',
            children=[
                html.H3('A simple card', className='card-title'),
                html.P('Card text', className='card-text'),
                html.A('Button', className='card-button')
            ]
        )
    ])


if __name__ == '__main__':
    app.run_server(debug=True)